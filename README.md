# bitodds-data-analysis

Jupyter notebooks demonstrating how to analyze [BitOdds](https://bitodds.com/) data.

To run these notebooks directly in your browser with no installation steps: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/BitEdge%2Fbitodds-data-analysis/HEAD)
