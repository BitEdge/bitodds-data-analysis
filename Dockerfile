FROM python:3.9-slim

COPY requirements.txt /tmp/
RUN pip install --no-cache --upgrade pip \
    && pip install --no-cache -r /tmp/requirements.txt

ARG NB_USER
ARG NB_UID
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}
RUN adduser \
    --disabled-password \
    --gecos "Default user" \
    --uid "${NB_UID}" \
    "${NB_USER}"
WORKDIR ${HOME}
USER ${USER}

COPY --chown=${NB_USER} *.ipynb ${HOME}/
COPY --chown=${NB_USER} data ${HOME}/data
